#include <iostream>
#include <iomanip>


//盤面の大きさを決める
const int Blimit = 8;

//盤面の状態を表す
//0　何もコマが置かれていない
//1　白が置かれている
//2　黒が置かれている
//3　コマが置ける場所である
const int komablank = 0;
const int white = 1;
const int black = 2;
const int guide = 3;

//配列の初期化
int board[Blimit][Blimit] = { { 0 } };

//ターン経過を表す変数の初期化
int turn = 1;

//置ける場所の数を表す変数の初期化
int guide_count = 0;

//パスが何度行われたかを表す変数の初期化
//guide_countが0の時（どこにも置けない時）加算される
//パスの後相手がコマを置くとリセットされ、カウントが2になるとゲーム終了
int pass = 0;

//白黒それぞれの数を数える変数の初期化
int white_count = 0;
int black_count = 0;

//CPUがコマを置いたかどうかを見る変数の初期化
int computer_put = 0;

//py,px		座標自体の値。プレイヤーが入力したり、初期化に使われたりする。
int py = 0;
int px = 0;

//gy,gx		盤面総当り用の座標
int gy = 0;
int gx = 0;

//総当り中のマスの周囲8マスを見るのに使う変数の初期化
//x = 0かつy = 0のときが探索中のマス
int y = -1;
int x = -1;

//盤面総当り用の変数gyとgxをそれぞれ代入するための変数
//代入した値にyとxの値を加算しひっくり返せるかどうかの探索につかう
int a = 0;
int b = 0;

//プレイヤー1とプレイヤー2を見る変数の初期化
//自分が1で相手が2
int p = 1;

//相手のコマを判別する変数を初期化
//1Pなら2を　2Pなら1で判別。
int c = 2;

//			書き方が悪かったので、すべて基本的にはy,xの順で処理。
//			方向を数値で表すと、
//			1 2 3
//			4 5 6
//			7 8 9

//クラスbanmenを作成
class banmen{
	//クラス外からも参照可能な変数や関数を書く
public:
	//盤面の初期化に使用する関数
	void shokika();

	//盤面の上下に横線を入れるための関数
	void kazari1();

	//盤面の左右に縦線をいれるための関数
	void kazari2();

	//ゲーム開始時にオセロの初期状態を作成するための関数
	void gamestart();

	//マスの状態を管理する関数
	void koma();

	//プレイヤーによる座標の入力を受け付け、判定を行うための関数
	void player();

	//プレイヤーの勝敗を判定する関数
	void winner();
};

//banmenクラスのshokika関数
//盤面をすべてコマが置かれていない状態にする
//gyが1増えるとxへの加算が始まり、右から左へ順番に初期化する
//横一列の初期化が終わるとまたyに加算され次の段へ進み同様の処理が行われる
//すべてのコマが初期化されるまで繰り返す
void banmen::shokika(){
	//座標gyがBlimit未満の間1を加算
	for (gy = 0; gy < Blimit; gy++)
	{
		//座標gxがBlimit未満の間1を加算

		for (gx = 0; gx < Blimit; gx++)
		{
			//現在の座標のマスを何も置かれていない状態にする
			board[gy][gx] = 0;
		}
	}
}

//banmenクラスのkawari1関数
//配置された場所からスペースとBlimitの値と同数の横線を並べる
void banmen::kazari1(){
	//空白を置く
	std::cout << "    ";
	//変数pxがBlimit未満の間pxに1を加算し続ける。
	for (px = 0; px < Blimit; px++)
	{
		//横線を一つ置く
		std::cout << "―";
	}
	//改行
	std::cout << std::endl;
}

//banmenクラスのkazari2関数
//おいた場所に縦線をひとつ置くだけの関数
//コメントを書いている現在、全く存在意義がわからない関数
void banmen::kazari2(){
	//縦線を一つ置く
	std::cout << "｜";

}

//banmenクラスのgamestart関数
//オセロ開始時の盤面を作る
void banmen::gamestart(){
	//リプレイ時に最後に使用した座標を初期化だと思われる
	//なくても動く気がする
	board[py][px] = 0;
	
	//オセロの一番最初に置くコマの配置
	//自分のコマをy3x4とy4x3に配置
	board[3][4] = board[4][3] = 1;

	//相手のコマをy3x3とy4x4に配置
	board[3][3] = board[4][4] = 2;
}

//banmenクラスのkoma関数
//盤面の状態を表す（コマが置かれているかどうか、コマが置けるかどうか）
void banmen::koma(){

	// switchでそれぞれの処理を切り替える。
	//もしも盤面の座標[py][px]が○○だったら
	switch (board[py][px])
	{
		//その場所にコマが置かれていない時
	case komablank:
		std::cout << "・";
		break;
		//その場所に自分のコマが置かれている時
	case white:
		std::cout << "○";
		break;
		//その場所に相手のコマが置かれている時
	case black:
		std::cout << "●";
		break;
		//その場所にコマが置ける時
	case guide:
		std::cout << "★";
		break;
		//その場所が上記どれにも属さない（エラー時）
	default:
		std::cout << "×";
		break;
	}
}

//checkguide関数
//自分、相手ターン時にそれぞれコマが置けるかどうかの判定を行う
//コマが置ける時にはガイド（★）を置く
void checkguide()
{
	//もしもターン数を2で割った余りが0なら（偶数ターン）
	if (turn % 2 == 0){
		//挟めた時にプレイヤーの色を相手の色で裏返すための変数への代入
		p = 1;
		c = 2;
	}
	//もしも余りが0ではないなら（奇数ターン）
	else {
		//挟めた時に相手の色を自分の色に裏返すための変数への代入
		p = 2;
		c = 1;
	}

	//gy,gx		総当り
	for (gy = 0; gy < Blimit; gy++)
	{
		for (gx = 0; gx < Blimit; gx++)
		{
			//ガイドがすでに置かれていたら
			if (board[gy][gx] == guide)
			{
				//そのマスを初期化する
				board[gy][gx] = 0;
			}

		}
	}
//----------------------------------------------------------------------

	//gy,gx		総当り
	for (gy = 0; gy < Blimit; gy++)
	{
		for (gx = 0; gx < Blimit; gx++)
		{

			//x,y		座標に加算する値
			for (y = -1; y <= 1; y++)
			{
				for (x = -1; x <= 1; x++)
				{
					//もしも探索中のマスを見ようとしたら一つとなりにずらす
					if (x == 0 && y == 0){ x++; }
					//変数a,bにそれぞれgy,gxを代入
					a = gy;
					b = gx;

					//もしも総当り中の座標の周囲に相手のコマがあったら
					if (board[a + y][b + x] == c){
						//同一の方向に相手のコマがあるかぎり探索
						while (board[a += y][b += x] == c){
							//けしちゃだめ
						}

						//探索終了時の座標がXYそれぞれ0以上かつBlimit未満のとき
						if (a >= 0 && a < Blimit && b >= 0 && b < Blimit)
						{
							//探索を終えた先がもし空白だったら探索開始時の値に戻す
							if (board[a][b] != p && board[a][b] != c){
								a = gy;
								b = gx;

							}
							//もし自分のコマに当たったら
							else if (board[a][b] == p){

								py = gy;
								px = gx;

								//コマが置かれていないのを確認して
								if (board[py][px] == 0){
									//ガイドを置く

									board[py][px] = guide;

								}
							}
						}
						//もしも探索した先の座標が0未満であれば
						else if (a < 0 || b < 0) {
							//処理を抜ける
							break;
						}

					}
				}
			}
		}
	}
//----------------------------------------------------------------------
}

//hantei関数
//ここで挟んだコマをひっくり返し実際にコマを置く
void hantei()
{
	//guide_countの値をゼロにする
	guide_count = 0;
//----------------------------------------------------------------------
	//x,y		座標に加算する値
	for (y = -1; y <= 1; y++)
	{
		for (x = -1; x <= 1; x++)
		{
			//もしも探索中のマスを見ようとしたら一つとなりにずらす
			if (x == 0 && y == 0){ x++; }
			//変数a,bにそれぞれgy,gxを代入
			a = py;
			b = px;

			//もしも置いた座標の周囲に相手のコマがあったら
			if (board[a + y][b + x] == c){

				//同一の方向に相手のコマがあるかぎり探索
				while (board[a += y][b += x] == c){
					//けしちゃだめ
				}

				//探索終了時の座標がXYそれぞれ0以上かつBlimit未満のとき
				if (a >= 0 && a < Blimit && b >= 0 && b < Blimit)
				{
					//探索を終えた先がもし空白だったら入力した値に戻す
					if (board[a][b] != p && board[a][b] != c){
						a = py;
						b = px;
					}
			
					//もし自分のコマに当たったら
					else if (board[a][b] == p){
						//その座標を表示（デバッグ用なのでなくてもうごく）
						std::cout << a + 1 << "," << b + 1 << std::endl;

						//置いた座標に当たるまでひっくり返す
						while (a != py || b != px){
							board[a -= y][b -= x] = p;
						}
						//そして入力した座標に自分のコマを置く
						board[py][px] = p;
					}

				}
			}
		}
	}
//----------------------------------------------------------------------
}

//banmenクラスのplayer関数
//ここで座標の入力や警告やおいた結果を文字で表示する
void banmen::player(){
	//gy,gx		総当り
	for (gy = 0; gy < Blimit; gy++)
	{
		for (gx = 0; gx < Blimit; gx++)
		{
			//もしも探索中の座標にガイドが置かれていたら
			if (board[gy][gx] == guide)
			{
				//guide_countに1加算する
				guide_count++;
			}

		}
	}
	//もしもguide_countが0のとき（置ける場所がない時）
	if (guide_count == 0){
		//自動パスする旨を表示
		std::cout << "どこにも置けないのでパスです" << std::endl;
		//pass回数に1加算
		pass++;
	}
	//もしも置ける場所があったら
	else{
		//プレイヤーが自分の時
		if (p == 1){
			//パス回数をリセットする
			pass = 0;
			//プレイヤーの順番をアナウンス
			std::cout << "プレイヤー" << p << "のターンです。1～8の間で入力してください" << std::endl;
			//処理が終わるまで無限ループ
			while (1){
				
				std::cout << "x座標を入力" << std::endl;
				//プレイヤーにpxの座標を入力させる
				std::cin >> px;
				//入力されたpxの値から-1する（数値が0スタートのため、入力できる値の1～8は今回の処理上0～7にしなくてはならないため）
				px -= 1;
				
				
				std::cout << "y座標を入力" << std::endl;
				
				//プレイヤーにpyの座標を入力させる
				std::cin >> py;
				//入力されたpyの値から-1する
				py -= 1;

				//もしも入力された値が半角数字でなかったら
				if (std::cin.fail()){
					//エラーをリセットする
					//リセットするまで次の入力を正常に受け付けてくれないため				
					std::cin.clear();
					//入力された文字を破棄する
					std::cin.ignore(1024, '\n');
					std::cout << "半角数字を入力して下さい。" << std::endl;
				}

				//半角数字であっても、盤面外の座標が入力されたら
				else if (px > Blimit || px < 0 || py > Blimit || py < 0)
				{
					std::cout << "xとyのどちらか、または両方が範囲外です" << std::endl;
				}
				//半角数字かつ盤面内の座標であっても、自分または相手のコマが置かれていたら
				else if (board[py][px] == 1 || board[py][px] == 2)
				{
					std::cout << "すでに置かれています" << std::endl;
				}
				//半角数字かつ盤面内かつ何も置かれていない座標であっても、ガイドが置かれていなかったら
				if (board[py][px] != guide) {
					std::cout << "そこには置けません" << std::endl;
				}
				//もし半角数字かつ盤面内でガイドが置かれている座標であれば
				else{
					//hantei関数の処理を実行
					hantei();
					//ループを抜ける
					break;
				}
			}
		}

		//プレイヤーがCPUのとき
		else if (p == 2){
			//パス回数をリセット
			pass = 0;
			std::cout << "雑魚のターンです" << std::endl;

			//コンピューターに左から右、上から下に順番に座標をチェックさせる
			for (py = 0; py < Blimit; py++)
			{
				for (px = 0; px < Blimit; px++)
				{
					//ガイドが置かれている座標にきたら
					if (board[py][px] == guide) {
						//hantei関数の処理を行う
						hantei();
						//computer_putに1加算
						computer_put++;
						//ループから抜ける
						break;
					}
				}
				//もしcomputer_putが1になっていたら
				if (computer_put == 1){
					//computer_putをリセットし
					computer_put = 0;
					//ループから抜ける
					break;
				}
			}
		}
		//改行
		std::cout << std::endl;
	}
}

//banmenクラスのwinner関数
//ゲーム終了時に呼び出し、盤面上のコマを数え勝敗を出力する
void banmen::winner(){
	//盤面を総当り
	for (gy = 0; gy < Blimit; gy++)
	{
		for (gx = 0; gx < Blimit; gx++)
		{
			//あたった座標が自分のコマだったら
			if (board[gy][gx] == white)
			{
				//white_countに1加算
				white_count++;
			}
			//あたった座標が相手のコマだったら
			if (board[gy][gx] == black)
			{
				//black_countに1加算
				black_count++;
			}
		}
	}
	//もしも自分のコマの数のほうが相手のコマよりも多ければ勝ち
	if (white_count > black_count){
		std::cout << white_count << "対" << black_count << "であなたの勝ちです！" << std::endl;
	}
	//もし自分のコマが相手の駒より少なければ負け
	else  if (white_count < black_count){
		std::cout << white_count << "対" << black_count << "であなたの負けです！" << std::endl;
	}
	//もし自分と相手のコマが同数だったら引き分け
	else  if (white_count == black_count){
		std::cout <<"引き分けです！" << std::endl;
	}

}

//main関数
//盤面の描画を主に行う
int main()
{
	//クラス関数を呼び出すためのbanmen型の変数oseroを作成
	//変数名.関数()のように使う
	banmen osero;

	//shokika関数の実行
	osero.shokika();
	
	//パスが2度行われるまでループする
	while (pass < 2){

		std::cout << turn - 1 << "ターン目" << std::endl;
		//ターン数が1以上なら
		if (turn >= 1){
			//checkguide関数を実行しガイドを設置する
			checkguide();
		}

		//スペース
		std::cout << "    ";
		//横一列分の数だけ行うループ
		for (px = 0; px < Blimit; px++)
		{
			//空白を右詰めで2文字分だけ半角スペースで埋め、pxに1を足した値を表示
			//座標が見やすいように番号を表示する
			std::cout << std::setfill(' ') << std::setw(2) << std::right << px + 1;
		}
		//改行
		std::cout << std::endl;
		
		//kazari1関数で上部横線を表示
		osero.kazari1();

		//盤面すべての座標似て行う処理
		//縦の座標を見る
		for (py = 0; py < Blimit; py++)
		{
			//空白を右詰めで2文字分だけ半角スペースで埋め、pyに1を足した値を表示
			//座標が見やすいように番号を表示する
			std::cout << std::setfill(' ') << std::setw(2) << std::right << py + 1;

			//kazari2関数で左端の縦線を表示
			osero.kazari2();

			//横の座標を見る
			for (px = 0; px < Blimit; px++){
				//もし1ターン目ならggamestart関数を実行する
				if (turn == 1){ osero.gamestart(); }
				//koma関数で盤面の状態を更新
				osero.koma();
			}
			//kazari2関数で右端の縦線を表示
			osero.kazari2();
			//改行
			std::cout << std::endl;
		}
		//kazari2関数で下部横線を表示
		osero.kazari1();
		//もしターンが2ターン目以上なら
		if (turn >= 2){
			//player関数でプレイヤーとCPUの入力を受け付ける
			osero.player();
		}
		//ループ内のすべての処理が終わったらturnに1加算する
		turn++;
	}
	//passの値が2になったらゲームループから抜け勝敗判定を行う
	//winner関数を実行し勝敗を判定
	osero.winner();

	//終了を表示する
	std::cout << "おつかれさまでした" << std::endl;
	std::cout << "Enterで終了します" << std::endl;
	getchar();
	getchar();

	//戻り値に0（偽）を返しウィンドウを閉じる
	return 0;
}
